<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE topic PUBLIC "-//OASIS//DTD DITA Topic//EN" "topic.dtd">
<topic id="topic_j52_lxp_rgb">
    <title>Syncro Soft Permissions Guidelines for Copyrighted Materials</title>
    <body>
        <p>Syncro Soft’s products, services, and Website content (including images, models, content
            screen shots, text, and software) are owned by Syncro Soft, SRL., and its licensors and
            may not be used without a license. Unless Syncro Soft explicitly gives you permission in
            the guidelines below or in another agreement between you and Syncro Soft (such as a
            Software License Agreement, Terms of Use, Terms of Service, or other written agreement),
            you may not use, reproduce, distribute, display, or perform such materials.</p>
        <section id="section_ft3_nxp_rgb">
            <title>Use of screen shots</title>
            <p>Screenshots are the individual screen displays contained within Syncro Soft software
                applications. You may make fair use of a limited number of screenshots for
                        <b><i>purely educational or illustrative purposes</i></b> (e.g., educational
                guides, tutorials, how-to books, training presentations, product reviews, websites,
                etc.) provided you adhere to the following guidelines:</p>
            <p>1. Your use may not directly or indirectly state or imply sponsorship, affiliation,
                or endorsement of your product or service by/with Syncro Soft.</p>
            <p>2. You may highlight or label a portion of a screenshot to call attention to a
                specific instruction or to explain a function of the software from which the
                screenshot is taken. You may crop or cut a portion of a screenshot to direct a
                learner’s attention to the function illustrated in that portion. You may not
                otherwise alter the screenshot in any way except to resize the screenshot in direct
                proportion to the original.</p>
            <p>3. You may add commentary or other text analysis as long as it is very clearly
                attributable to you, and not to Syncro Soft.</p>
            <p>4. You may not use screenshots from Syncro Soft beta products or other products that
                have not yet been commercially released by Syncro Soft.</p>
            <p>5. You may not use screenshots that contain third-party content unless you separately
                seek and obtain permission from such third party to display its content.</p>
            <p>6. You must include the following attribution statement on all materials containing
                Syncro Soft screenshots: "Oxygen screenshots reprinted courtesy of Syncro Soft."</p>
            <p>7. If your use includes references to any Syncro Soft trademarks, you must include
                the following trademark attribution statement: "[List of marks used in alphabetical
                order] are either registered trademarks or trademarks of Syncro Soft in the United
                States and/or other countries."</p>
            <p>8. Your use of Syncro Soft screenshots may not be incorporated into obscene or
                pornographic material, and may not, in the sole opinion of Syncro Soft, be
                disparaging, defamatory, or libelous to Syncro Soft, any of its products, or any
                other person or entity.</p>
            <p>9. Your materials should <b>not</b> be mostly or solely composed of Syncro Soft
                screenshots or other Syncro Soft intellectual property. Instead, your materials
                should add substantial commentary or value to the Syncro Soft screenshots or other
                Syncro Soft intellectual property.</p>
            <p>If your use meets all the above criteria, no written permission is required. All
                other use of Syncro Soft screenshots requires prior written permission from Syncro
                Soft. If further written permission is required, please send a letter detailing your
                request to <xref href="mailto:sales@oxygenxml.com" format="com" scope="external"
                    >sales@oxygenxml.com</xref>.</p>
        </section>
        <section id="section_jt3_nxp_rgb">
            <title>Use of Product User Guides</title>
            <p>Syncro Soft product documentation is the product documentation distributed by Syncro
                Soft in conjunction with Syncro Soft software applications. Syncro Soft does not
                object to your use of short portions of Syncro Soft-authored user guides for
                        <b><i>purely educational/instructional purposes only</i></b>, provided you
                adhere to all of the following guidelines:</p>
            <p>1. Your use may not directly or indirectly imply sponsorship, affiliation, or
                endorsement of your product or service by/with Syncro Soft.</p>
            <p>2. You may reproduce no more than three chapters or other major sections of any
                Syncro Soft product user guide, and such chapters may only be distributed to your
                students or clients for educational or illustrative purposes.</p>
            <p>3. If you are distributing portions of Syncro Soft product documentation as a handout
                (and not incorporating such portions as part of a larger work), the Syncro Soft
                copyright page of any corresponding product user guide must accompany all such
                copied portions, and you may not identify yourself as being the author of such
                material.</p>
            <p>4. If you are incorporating portions of Syncro Soft-authored product documentation as
                part of a larger work, you must include the following copyright attribution
                statement in a conspicuous location with the copied portions: “© [year of copyright
                notice of corresponding product user guide] Syncro Soft SRL. Reprinted with the
                permission of Syncro Soft SRL.” You may not identify yourself as being the author of
                such copied portions.</p>
            <p>5. You may not alter Syncro Soft-authored product documentation, or any material
                contained therein, in any way.</p>
            <p>6. Your use may not be incorporated with or into any obscene or pornographic
                material, and may not be disparaging, defamatory, or libelous to Syncro Soft, any of
                its products, or any other person or entity.</p>
            <p>7. You may not separately sell any portion of any Syncro Soft product documentation
                for a profit.</p>
            <p>If your use meets the above criteria, <b>no further written permission is
                    required</b>. All other use of Syncro Soft product documentation requires prior
                written permission from Syncro Soft by sending a letter detailing your request to
                    <xref href="mailto:sales@oxygenxml.com" format="com" scope="external"
                    >sales@oxygenxml.com</xref>. Please note that Syncro Soft product documentation
                does not include Syncro Soft official training guides, which may not be used,
                reproduced, or distributed without prior written permission from Syncro Soft.</p>
        </section>
    </body>
</topic>
